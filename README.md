# Godot Accessibility Plugin

This plugin implements a simplistic screen reader for user interfaces created with the [Godot game engine](https://godotengine.org). The goal is to enable the creation of [audio games](https://en.wikipedia.org/wiki/Audio_game) with Godot, as well as to add accessibility functionality to traditional UI-centric games.
